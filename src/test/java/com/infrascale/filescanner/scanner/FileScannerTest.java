package com.infrascale.filescanner.scanner;

import com.infrascale.filescanner.model.FileInfo;
import org.junit.Test;

import java.io.File;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Oleg.Sotnichenko on 16.12.2014.
 */
public class FileScannerTest {
    @Test
    public void testScan() throws Exception {
        File file = new File(getClass().getClassLoader().getResource("files").getFile());

        FileScanner fileScanner = new FileScanner();
        Collection<FileInfo> result = fileScanner.scan(file);
        assertNotNull(result);

        long[] sizes = result.stream().mapToLong(FileInfo::getSize).sorted().toArray();
        assertArrayEquals(new long[]{1L, 2L, 3L}, sizes);
    }


    @Test
    public void testScanNotExistingFile() throws Exception {
        File file = new File(getClass().getClassLoader().getResource("files").getFile() + "error");

        FileScanner fileScanner = new FileScanner();
        Collection<FileInfo> result = fileScanner.scan(file);
        assertNotNull(result);

        assertTrue(result.isEmpty());
    }
}
