package com.infrascale.filescanner.histogram;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by oleg on 17.12.14.
 */
public class HistogramTest {
    @Test
    public void testCalculate1() throws Exception {
        List<Long> values = Arrays.asList(10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L);
        Histogram histogram = new Histogram();
        List<Long> result = histogram.calculate(values, 1L, 100L, 10L);
        Assert.assertArrayEquals(new Long[]{1L,1L,1L,1L,1L,1L,1L,1L,1L,1L}, result.toArray());
    }

    @Test
    public void testCalculate2() throws Exception {
        List<Long> values = Arrays.asList(20L, 20L, 40L, 40L, 60L, 60L, 70L, 70L, 100L);
        Histogram histogram = new Histogram();
        List<Long> result = histogram.calculate(values, 1L, 100L, 10L);
        Assert.assertArrayEquals(new Long[]{0L,2L,0L,2L,0L,2L,2L,0L,0L,1L}, result.toArray());
    }

    @Test
    public void testCalculate3() throws Exception {
        List<Long> values = Arrays.asList(10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L);
        Histogram histogram = new Histogram();
        List<Long> result = histogram.calculate(values, 31L, 100L, 10L);
        Assert.assertArrayEquals(new Long[]{1L,1L,1L,1L,1L,1L,1L}, result.toArray());
    }

    @Test
    public void testCalculate4() throws Exception {
        List<Long> values = Arrays.asList(10L, 20L, 30L, 30L, 40L, 50L, 50L, 60L, 70L, 90L, 100L, 100L, 110L);
        Histogram histogram = new Histogram();
        List<Long> result = histogram.calculate(values, 31L, 100L, 10L);
        Assert.assertArrayEquals(new Long[]{1L, 2L,1L,1L,0L,1L,2L}, result.toArray());
    }

    @Test
    public void testCalculateInterval() throws Exception {
        Histogram histogram = new Histogram();
        int interval = histogram.calculateInterval(0.0, 100.0, 10.0);
        Assert.assertEquals(10.0, interval, 0.001);
    }
}
