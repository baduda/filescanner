package com.infrascale.filescanner.scanner;

import com.google.common.io.Files;
import com.infrascale.filescanner.model.FileInfo;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;

import javax.inject.Named;
import java.io.File;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by Oleg.Sotnichenko on 16.12.2014.
 */
@Named
public class FileScanner {

    public final boolean isWindowsOs;

    public FileScanner() {
        isWindowsOs = System.getProperty("os.name").toLowerCase().startsWith("windows");
    }

    public Collection<FileInfo> scan(File directory) {
        return StreamSupport.stream(
                Files.fileTreeTraverser().breadthFirstTraversal(directory).spliterator(), false).
                filter(File::isFile).
                map(f -> new FileInfo(f.length(), isSparse(f))).
                collect(Collectors.toSet());
    }

    //windows specific operation
    public boolean isSparse(File file) {
        return isWindowsOs &&
                (Kernel32.INSTANCE.GetFileAttributes(file.getAbsolutePath()) & WinNT.FILE_ATTRIBUTE_SPARSE_FILE) != 0;
    }
}
