package com.infrascale.filescanner.presentation;

import com.infrascale.filescanner.model.Unit;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import lombok.val;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.inject.Named;
import javax.swing.*;
import java.util.Collections;
import java.util.List;

/**
 * Created by Oleg.Sotnichenko on 17.12.2014.
 */
@Named
public class MainPanel {
    private JButton searchButton = new JButton("Search");
    private JButton fileChooserButton = new JButton("...");
    private JTextField pathInput = new JTextField();
    private JTextField minInput = new JTextField();
    private JTextField maxInput = new JTextField();
    private JTextField intervalInput = new JTextField();
    private JComboBox<Unit> unitInput = new JComboBox<>(Unit.values());

    private JLabel numberLabel = new JLabel("0");
    private JLabel sizeLabel = new JLabel("0");
    private JLabel sparseLabel = new JLabel("0");

    private ChartPanel chartPanel = new ChartPanel(new JFreeChart(new XYPlot()));

    public JComponent buildPanel() {
        val layout = new FormLayout(
                "right:20dlu, 1dlu, 50dlu, 1dlu, " +
                        "right:20dlu, 1dlu, 50dlu, 1dlu, " +
                        "right:20dlu, 1dlu, 50dlu, 1dlu, " +
                        "right:20dlu, 1dlu, 50dlu, 1dlu, " +
                        "right:20dlu, 1dlu, 50dlu, 1dlu, " +
                        "right:20dlu, 1dlu, 50dlu, 1dlu",
                "");
        val builder = new DefaultFormBuilder(layout);
        builder.border(Borders.DIALOG);

        //search panel
        builder.appendSeparator("Search");
        builder.append("Path", pathInput);
        builder.append("", fileChooserButton);
        builder.append("Min", minInput);
        builder.append("Max", maxInput);
        builder.append("Step", intervalInput);
        builder.append("Unit", unitInput);

        builder.append("", searchButton, 2);
        builder.nextLine();

        //statistic panel
        builder.appendSeparator("Results");
        builder.append("Num:", numberLabel);
        builder.append("Size:");
        builder.append(sizeLabel,3);
        builder.append("Sparse:", sparseLabel);
        builder.nextLine();

        //histogram panel
        builder.appendSeparator("Histogram");
        builder.append(chartPanel, 23);

        updateChart(Collections.emptyList(), 0L, Unit.K.getBytes(), Unit.K);
        return builder.getPanel();
    }

    public void updateChart(List<Long> values, Long min, Long max, Unit unit) {
        val series = new XYSeries("Random Data");

        double interval;
        if(values.isEmpty()){
            interval = 1.0;
        } else {
            interval = (max - min) / values.size() / unit.getBytes();
        }

        double x = min / unit.getBytes() + interval/2;
        for (Long value : values) {
            series.add(x, (double)value);
            x += interval;
        }
        XYSeriesCollection dataset = new XYSeriesCollection(series);
        dataset.setAutoWidth(true);

        chartPanel.setChart(ChartFactory.createXYBarChart(
                "File Sizes Histogram",
                "size",
                false,
                "amount",
                dataset,
                PlotOrientation.VERTICAL,
                false,
                false,
                false
        ));
    }

    public JButton getSearchButton() {
        return searchButton;
    }

    public JTextField getPathInput() {
        return pathInput;
    }

    public JTextField getMinInput() {
        return minInput;
    }

    public JTextField getMaxInput() {
        return maxInput;
    }

    public JTextField getIntervalInput() {
        return intervalInput;
    }

    public JComboBox<Unit> getUnitInput() {
        return unitInput;
    }

    public JLabel getNumberLabel() {
        return numberLabel;
    }

    public JLabel getSizeLabel() {
        return sizeLabel;
    }

    public JLabel getSparseLabel() {
        return sparseLabel;
    }

    public ChartPanel getChartPanel() {
        return chartPanel;
    }

    public JButton getFileChooserButton() {
        return fileChooserButton;
    }
}
