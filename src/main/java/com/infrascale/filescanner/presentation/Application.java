package com.infrascale.filescanner.presentation;

import com.google.common.base.Strings;
import com.infrascale.filescanner.histogram.Histogram;
import com.infrascale.filescanner.model.FileInfo;
import com.infrascale.filescanner.model.Unit;
import com.infrascale.filescanner.scanner.FileScanner;
import lombok.val;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Oleg.Sotnichenko on 16.12.2014.
 */
@Named
public class Application {
    @Inject
    private MainPanel mainPanel;
    @Inject
    private Histogram histogram;
    @Inject
    private FileScanner fileScanner;

    private JFrame frame;

    @PostConstruct
    private void init() {
        initComponents(mainPanel);
        frame = createFrame();
    }

    private JFrame createFrame() {
        val frame = new JFrame();
        frame.setTitle("File Scanner");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel.buildPanel());
        frame.setLocationByPlatform(true);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
        return frame;
    }

    private List<Long> calculateHistogram(Collection<FileInfo> fileInfos, Long minValue, Long maxValue, Long interval) {
        val values = fileInfos.stream().mapToLong(FileInfo::getSize).boxed().collect(Collectors.toList());

        //default min, max and interval
        if (maxValue == 0L && !values.isEmpty()) {
            maxValue = Collections.max(values);
        }
        if (interval == 0L) {
            interval = (maxValue - minValue) / 20;
        }

        return histogram.calculate(values, minValue, maxValue, interval);
    }

    private void initComponents(MainPanel mainPanel) {
        mainPanel.getSearchButton().addActionListener(e -> SwingUtilities.invokeLater(this::searchFileAction));
        mainPanel.getFileChooserButton().addActionListener(e -> SwingUtilities.invokeLater(this::fileChooserAction));
    }

    private void fileChooserAction() {
        val chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            mainPanel.getPathInput().setText(chooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void searchFileAction() {
        try {
            val unit = (Unit) mainPanel.getUnitInput().getSelectedItem();

            val min = convertToBytes(mainPanel.getMinInput(), unit);
            val max = convertToBytes(mainPanel.getMaxInput(), unit);
            val interval = convertToBytes(mainPanel.getIntervalInput(), unit);
            val root = new File(mainPanel.getPathInput().getText());

            if (!validate(min, max, interval, root)) {
                return;
            }

            val cursor = frame.getCursor();
            frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            val results = fileScanner.scan(root);
            frame.setCursor(cursor);
            updateScanResults(results, min, max, interval, unit);

        } catch (NumberFormatException exception) {
            showErrorIfNotBlank("Not numeric value");
        }
    }

    private Long convertToBytes(JTextField textField, Unit unit) {
        return Long.valueOf(textField.getText()) * unit.getBytes();
    }

    private boolean validate(Long min, Long max, Long interval, File file) {
        String errorMessage = null;
        if (!file.exists()) {
            errorMessage = "Invalid path";
        } else if (max < min) {
            errorMessage = "Max value less them min";
        } else if (interval <= 0 || interval > max - min) {
            errorMessage = "Incorrect interval";
        }
        return !showErrorIfNotBlank(errorMessage);

    }

    private boolean showErrorIfNotBlank(String errorMessage) {
        if (Strings.isNullOrEmpty(errorMessage)) {
            return false;
        }
        JOptionPane.showMessageDialog(frame, errorMessage, "Validation error", JOptionPane.ERROR_MESSAGE);
        return true;
    }

    private void updateScanResults(Collection<FileInfo> fileInfos, Long minValue, Long maxValue, Long interval, Unit unit) {
        final Integer totalCount = fileInfos.size();
        final Long totalSize = fileInfos.stream().mapToLong(FileInfo::getSize).sum();
        final Long sparseCount = fileInfos.stream().filter(FileInfo::isSparse).count();

        mainPanel.getNumberLabel().setText(totalCount.toString());
        mainPanel.getSizeLabel().setText(totalSize.toString() + " b");
        mainPanel.getSparseLabel().setText(sparseCount.toString());

        val values = calculateHistogram(fileInfos, minValue, maxValue, interval);
        mainPanel.updateChart(values, minValue, maxValue, unit);
    }
}