package com.infrascale.filescanner.histogram;

import lombok.val;

import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.lang.Math.ceil;

/**
 * Created by oleg on 17.12.14.
 */
@Named
public class Histogram {

    public List<Long> calculate(final Collection<Long> values, final Long min, final Long max, final Long interval) {
        val intervalCount = calculateInterval(min, max, interval);
        val result = new Long[intervalCount];
        Arrays.fill(result, 0L);
        values.stream().
                mapToInt(v -> calculateInterval(min, v, interval)-1).
                filter(i-> 0 <= i && i < intervalCount).
                forEach(i -> result[i]++);
        return Arrays.asList(result);
    }

    public int calculateInterval(double min, double max, double interval) {
        return (int) ceil((max - min) / interval);
    }


}
