package com.infrascale.filescanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.swing.*;

/**
 * Created by oleg on 23.12.14.
 */
@Configuration
@ComponentScan("com.infrascale.filescanner")
public class ApplicationConfiguration {

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel("ch.randelshofer.quaqua.leopard.Quaqua15LeopardCrossPlatformLookAndFeel");
        new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
    }
}
