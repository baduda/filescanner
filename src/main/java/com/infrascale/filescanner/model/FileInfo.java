package com.infrascale.filescanner.model;

import lombok.Data;

/**
 * Created by Oleg.Sotnichenko on 16.12.2014.
 */
@Data
public class FileInfo {
    private final long size;
    private final boolean sparse;
}
