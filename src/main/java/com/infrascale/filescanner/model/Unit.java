package com.infrascale.filescanner.model;

import lombok.Getter;

/**
 * Created by Oleg.Sotnichenko on 17.12.2014.
 */
@Getter
public enum Unit {
    B("b", 1), K("K", 1024), MB("MB", 1024*1024), GB("GB", 1024*1024*1024);

    private final String label;
    private final long bytes;

    Unit(String label, long bytes) {
        this.label = label;
        this.bytes = bytes;
    }

    @Override
    public String toString() {
        return label;
    }
}
